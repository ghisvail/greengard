function fk = dirft3_type1(xj, yj, cj, ms, mt, mu, varargin)
% function fk = dirft3_type1(xj, yj, zj, cj, ms, mt, mu[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   zj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   cj: vector-like, complex
%Complex value of the non-uniform samples at locations xj.
%   ms: scalar, ms > 2
%Dimensions of the uniform grid.
%   mt: scalar, mt > 2
%Dimensions of the uniform grid.
%   mu: scalar, mu > 2
%Dimensions of the uniform grid.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   fk: matrix-like, complex, shape [ms, mt, mu]
%NUFFT'd data on the uniform grid
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
