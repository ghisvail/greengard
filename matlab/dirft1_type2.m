function fk = dirft1_type2(xj, fk, varargin)
% function fk = dirft1_type2(xj, fk[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   fk: vector-like, complex, shape [ms > 2]
%Complex value of the uniform samples.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   cj: vector-like, complex
%NUFFT'd data at locations xj
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
