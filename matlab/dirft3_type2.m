function fk = dirft3_type2(xj, yj, zj, fk, varargin)
% function fk = dirft3_type2(xj, yj, zj, fk[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   zj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   fk: matrix-like, complex, shape [ms > 2, mt > 2, mu > 2]
%Complex value of the uniform samples.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   cj: vector-like, complex
%NUFFT'd data at locations (xj, yj, zj)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
