function fk = nufft2_type3(xj, yj, cj, sk, tk, varargin)
% function fk = nufft2_type3(xj, yj, cj, sk, tk[, iflag, eps])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform input samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform input samples normalized in [0, 2*pi).
%   cj: vector-like, complex
%Complex value of the non-uniform samples at locations xj.
%   sk: vector-like
%Coordinates of the non-uniform output samples normalized in [0, 2*pi).
%   tk: vector-like
%Coordinates of the non-uniform output samples normalized in [0, 2*pi).
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%   eps: scalar, optional, default 1e-6, 1e-13 <= eps <= 1e-1
%Desired precision, use 1e-6 for single, 1e-12 for double.
%
%Returns
%-------
%   fk: vector-like, complex
%NUFFT'd data at locations (sk, tk)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
