#include "fintrf.h"
!=======================================================================
!   
!     nufft2_type2.F90
!
!     Fortran wrapper for the Type-2 NUFFT in 2D
!      
!     Created by Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
!=======================================================================
        subroutine mexFunction(nlhs, plhs, nrhs, prhs)
!       cj = nufft2_type2(xj, yj, fk[, iflag, eps])

        implicit none
        
        mwPointer plhs(*), prhs(*)
        integer nlhs, nrhs
        
        mwPointer mxGetPr, mxGetPi, mxCreateDoubleMatrix
        mwSize mxGetM, mxGetN, mxGetNumberOfElements
        integer mxIsNumeric, mxIsComplex
        
        mwPointer xj, yj
        real*8 eps
        complex*16, allocatable :: cj(:), fk(:,:)
        integer nj, iflag, ms, mt, ier
        
        mwSize m, n        
        integer dummy_nj, iarg
        real*8 dummy_real
        
!-----------------------------------------------------------------------
!       Sanity checks

!       Check for proper number of arguments
        if(nrhs < 3) then
            call mexErrMsgTxt('Function requires at least 3 input &
            arguments.')
        elseif(nlhs /= 1) then
            call mexErrMsgTxt('Function requires 1 output argument.')
        endif

!       Manage input arguments
!       prhs(1) -> xj
        iarg = 1
        if(mxIsNumeric(prhs(iarg)) /= 1) then
            call mexErrMsgTxt('Input parameter 1 should be numeric.')
        endif
        
        xj = mxGetPr(prhs(iarg))
        nj = mxGetNumberOfElements(prhs(iarg))

!       prhs(2) -> yj
        iarg = 2
        if(mxIsNumeric(prhs(iarg)) /= 1) then
            call mexErrMsgTxt('Input parameter 2 should be numeric.')
        endif

        dummy_nj = mxGetNumberOfElements(prhs(iarg))
        
        if(dummy_nj /= nj) then
            call mexErrMsgTxt('Input parameters 1 and 2 should be of same &
            size.')  
        endif

        yj = mxGetPr(prhs(iarg))
 
!       prhs(3) -> fk
        iarg = 3
        m = mxGetM(prhs(iarg))
        n = mxGetN(prhs(iarg))
        if(mxIsComplex(prhs(iarg)) /= 1) then
            call mexErrMsgTxt('Input parameter 3 should be complex.')
        elseif(.not.(m >= 1 .and. n >= 1)) then
            call mexErrMsgTxt('Input parameter 3 should be a 2D matrix.')     
        endif
        
        ms = m
        
        if(ms < 2) then
            call mexErrMsgTxt('Grid dimension should be at least 2')
        endif        
        
        mt = n
        
        if(mt < 2) then
            call mexErrMsgTxt('Grid dimension should be at least 2')
        endif        
        
!       prhs(4) -> iflag (optional)
        if(nrhs > 3) then
            iarg = 4
            m = mxGetM(prhs(iarg))
            n = mxGetN(prhs(iarg))
            if(mxIsNumeric(prhs(iarg)) /= 1) then
                call mexErrMsgTxt('Input parameter 4 should be numeric.')
            elseif(.not.(m == 1 .and. n == 1)) then
                call mexErrMsgTxt('Input parameter 4 should be a scalar.')
            endif
            
            call mxCopyPtrToReal8(mxGetPr(prhs(iarg)), dummy_real, 1)
            iflag = int(dummy_real)
        else 
            iflag = -1
        endif

!       prhs(5) -> eps (optional)
        if(nrhs > 4) then
            iarg = 5
            m = mxGetM(prhs(iarg))
            n = mxGetN(prhs(iarg))
            if(mxIsNumeric(prhs(iarg)) /= 1) then
                call mexErrMsgTxt('Input parameter 5 should be numeric.')
            elseif(.not.(m == 1 .and. n == 1)) then
                call mexErrMsgTxt('Input parameter 5 should be a scalar.')
            endif
            
            call mxCopyPtrToReal8(mxGetPr(prhs(iarg)), eps, 1)
            
            if ((eps < 1d-13) .or. (eps > 1d-1)) then
                call mexErrMsgTxt('Precision value should be in &
                [1e-13, 1e-1].')
            endif
        else 
            eps = 1d-6
        endif        
        
!-----------------------------------------------------------------------
!       Memory management for complex data

        allocate(fk(ms, mt), stat=ier)
        if(ier /= 0) then
            call mexErrMsgTxt('Memory allocation failed.')
        else
            iarg = 3
            call mxCopyPtrToComplex16(mxGetPr(prhs(iarg)), &
            mxGetPi(prhs(iarg)), fk, ms*mt) 
        endif
        
        allocate(cj(nj), stat=ier)
        if(ier /= 0) then
            call mexErrMsgTxt('Memory allocation failed.')
        endif        

!-----------------------------------------------------------------------
!       Call computational routine

        call nufft2d2f90(nj, %val(xj), %val(yj), cj, iflag, eps, ms, mt, fk, &
        ier)

!-----------------------------------------------------------------------        
!       Error check

!       error code does not need to be processed since we already
!       checked for valid precision value

!       Memory management for complex data
        plhs(1) = mxCreateDoubleMatrix(1, nj, 1)
        call mxCopyComplex16ToPtr(cj, mxGetPr(plhs(1)), &
        mxGetPi(plhs(1)), nj)

        deallocate(cj, stat=ier)
        if(ier /= 0) then
            call mexErrMsgTxt('Memory release failed.')
        endif
        
        deallocate(fk, stat=ier)
        if(ier /= 0) then
            call mexErrMsgTxt('Memory release failed.')
        endif

        return
        end subroutine
