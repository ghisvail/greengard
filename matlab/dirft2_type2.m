function fk = dirft2_type2(xj, yj, fk, varargin)
% function fk = dirft2_type2(xj, yj, fk[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   fk: matrix-like, complex, shape [ms > 2, mt > 2]
%Complex value of the uniform samples.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   cj: vector-like, complex
%NUFFT'd data at locations (xj, yj)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
