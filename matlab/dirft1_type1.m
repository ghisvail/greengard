function fk = dirft1_type1(xj, cj, ms, varargin)
% function fk = dirft1_type1(xj, cj, ms[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   cj: vector-like, complex
%Complex value of the non-uniform samples at locations xj.
%   ms: scalar, ms > 2
%Dimensions of the uniform grid.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   fk: vector-like, complex
%NUFFT'd data on the uniform grid
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
