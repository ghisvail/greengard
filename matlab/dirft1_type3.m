function fk = dirft1_type3(xj, cj, sk, varargin)
% function fk = dirft1_type3(xj, cj, sk[, iflag])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform input samples normalized in [0, 2*pi).
%   cj: vector-like, complex
%Complex value of the non-uniform samples at locations xj.
%   sk: vector-like
%Coordinates of the non-uniform output samples normalized in [0, 2*pi).
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%
%Returns
%-------
%   fk: vector-like, complex
%NUFFT'd data at locations sk
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
