function fk = nufft2_type2(xj, yj, fk, varargin)
% function fk = nufft2_type2(xj, yj, fk[, iflag, eps])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   fk: matrix-like, complex, shape [ms > 2, mt > 2]
%Complex value of the uniform samples.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%   eps: scalar, optional, default 1e-6, 1e-13 <= eps <= 1e-1
%Desired precision, use 1e-6 for single, 1e-12 for double.
%
%Returns
%-------
%   cj: vector-like, complex
%NUFFT'd data at locations (xj, yj)
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
