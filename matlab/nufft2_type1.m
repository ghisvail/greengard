function fk = nufft2_type1(xj, yj, cj, ms, mt, varargin)
% function fk = nufft2_type1(xj, yj, cj, ms, mt[, iflag, eps])
%
%Parameters
%----------
%   xj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   yj: vector-like
%Coordinates of the non-uniform samples normalized in [0, 2*pi).
%   cj: vector-like, complex
%Complex value of the non-uniform samples at locations xj.
%   ms: scalar, ms > 2
%Dimensions of the uniform grid.
%   mt: scalar, mt > 2
%Dimensions of the uniform grid.
%   iflag: scalar, optional, default -1
%Sign of the Fourier transform.
%   eps: scalar, optional, default 1e-6, 1e-13 <= eps <= 1e-1
%Desired precision, use 1e-6 for single, 1e-12 for double.
%
%Returns
%-------
%   fk: matrix-like, complex, shape [ms, mt]
%NUFFT'd data on the uniform grid
%
% Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>
