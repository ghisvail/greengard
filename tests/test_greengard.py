# -*- coding: utf-8 -*-
"""
Test suite for nuffpack, based on original Greengard demo files

@author: localgv10
"""

# imports
import numpy as np
from numpy import pi
from numpy.testing import assert_raises
from greengard import *

TESTED_EPS = (1e-1, 1e-6, 1e-12)
INVALID_EPS = (1.0, 1e-14, 1e6)

class TestNufft1D(object):

    def setUp(self):
        """This method is run once before _each_ test method is executed"""
        self.ms = 90
        self.nj = 128        
        jj = np.arange(self.nj)
        self.xj = pi * np.cos(-pi * jj / self.nj)
        self.cj = np.sin(pi * jj / self.nj) + 1j * np.cos(pi * jj / self.nj)
        self.fk0 = dirft1_type1(self.xj, self.cj, self.ms, -1)
        self.cj0 = dirft1_type2(self.xj, self.fk0, +1)
        kk = np.arange(self.ms)
        self.sk = pi * np.sin(3 * pi * kk / self.ms)
        self.fk_sk0 = dirft1_type3(self.xj, self.cj, self.sk, -1)

    def test_type1(self):
        for eps in TESTED_EPS:
            fk1 = nufft1_type1(self.xj, self.cj, self.ms, -1, eps)
            assert(check_accuracy(fk1, self.fk0, eps))

    def test_type2(self):
        for eps in TESTED_EPS:
            cj1 = nufft1_type2(self.xj, self.fk0, +1, eps)
            print np.abs(self.cj0 - cj1).mean()
            assert(check_accuracy(cj1, self.cj0, eps))

    def test_type3(self):
        for eps in TESTED_EPS:
            fk_sk1 = nufft1_type3(self.xj, self.cj, self.sk, -1, eps)
            print np.abs(self.fk_sk0 - fk_sk1).mean()
            assert(check_accuracy(fk_sk1, self.fk_sk0, eps))

    def test_invalid_precision(self):
        for eps in INVALID_EPS:
            args_list = [self.xj, self.cj, self.ms, -1, eps]
            assert_raises(Exception, nufft1_type1, args_list)
            args_list = [self.xj, self.fk0, +1, eps]
            assert_raises(Exception, dirft1_type2, args_list)
            args_list = [self.xj, self.cj, self.sk, -1, eps]
            assert_raises(Exception, nufft1_type3, args_list)


def check_accuracy(f, f0, eps):
    """
    check that the mean residual error falls within the order of the 
    expected accuracy
    """
    return (np.abs(f - f0).mean() / eps) < 10


if __name__ == '__main__':
    import nose
    nose.run(argv=[__file__, '-vv'])
