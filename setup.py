#!/usr/bin/env python
# Author: Ghislain Vaillant
#
# Adapted from Pearu Peterson's setup.py in scipy.fftpack


def configuration(parent_package='', top_path=None):
    from numpy.distutils.misc_util import Configuration

    config = Configuration('greengard', parent_package, top_path)

    sources = ['greengard/greengard.pyf', 'lib/*.f']

    config.add_extension('greengard', sources=sources)
    return config


if __name__ == '__main__':
    from numpy.distutils.core import setup
    setup(description="greengard - Python wrapper for Greengard's NUFFT",
          author='Ghislain Vaillant',
          author_email='ghislain.vaillant@kcl.ac.uk',
          license='GPL',
          **configuration(top_path='').todict())
