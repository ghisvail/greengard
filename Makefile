# Makefile for the MATLAB wrappers around Greengard's NUFFT package
#
# Created by Ghislain Vaillant <ghislain.vaillant@kcl.ac.uk>

BUILD_DIR = ./build
MAT_DIR = ./matlab
SRC_DIR = ./lib
SRC_FILES = $(wildcard $(SRC_DIR)/*.f)
MEX = mex
FLAGS = -outdir $(BUILD_DIR)

PROG_DIRFT = dirft1_type1 dirft1_type2 dirft1_type3 dirft2_type1 dirft2_type2 dirft2_type3 dirft3_type1 dirft3_type2 dirft3_type3
PROG_NUFFT = nufft1_type1 nufft1_type2 nufft1_type3 nufft2_type1 nufft2_type2 nufft2_type3 nufft3_type1 nufft3_type2 nufft3_type3

all: $(PROG_DIRFT) $(PROG_NUFFT)
dirft: $(PROG_DIRFT)
nufft: $(PROG_NUFFT)

dirft1_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft1_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft1_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft2_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft2_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft2_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft3_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft3_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
dirft3_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft1_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft1_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft1_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft2_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft2_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft2_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft3_type1:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft3_type2:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
nufft3_type3:
	$(MEX) $(FLAGS) $(MAT_DIR)/$@.F90 $(SRC_FILES)
	cp $(MAT_DIR)/$@.m $(BUILD_DIR)
